<?php include('session.php'); ?>

<!DOCTYPE html>
<html>
<head>
	<title>demo</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	
	<link rel="stylesheet" type="text/css" href="css/sweetalert.css" />
	<script src="js/sweetalert.min.js" type="text/javascript"></script>
	
</head>
<body>

<header>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand logo" href="index.html"><img src="image/logo.png" alt="" /></a>
		</div>
		<ul class="nav navbar-nav pull-right">
			<li><a href="#">Products</a></li>
			<li><a href="#">Support</a></li>
			<li><a href="#">Developers</a></li>
			<li><a href="#">About</a></li>
			<li><a href="#">Contact Us</a></li>
			<li><a href="#">Blog</a></li>
		</ul>
	</div>
</nav>
</header>

<div class="container">
<div class="col-lg-12 col-md-12 col-sm-12 pull-left minHeight">
	<div class="col-lg-8 col-md-8 col-sm-8 pull-left">
		<div class="remoteStyle" id="remote" autoplay></div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 pull-left ">
		<div class="localStyle" id="local" autoplay></div>
		<form>
			<div class="col-lg-12 col-md-12 col-sm-12 pull-left">
			<div class="row">
			
			<div class="form-group col-lg-8 col-md-8 pull-left">
			<div class="row">
				<input type="text" id="dialNo" value="" placeholder="Enter Number" class="form-control input-md" />
			</div>
			</div>
			
			<div class="col-lg-3 col-md-3 pull-right">
			<div class="row">
				<button type="submit" class="btn btn-default fullwidth" id="dial">Dial</button>
			</div>
			</div>
			
			</div>
			</div>
			<button type="button" id="hangup" class="btn btn-danger">Call End</button>
			<!-- <button type="button" id="hold" class="btn btn-warning">Hold</button>
			<button type="button" id="resume" class="btn btn-success">Resume</button>
			<button type="button" id="mute" class="btn btn-info"><span class="glyphicon glyphicon-volume-off"></span>Mute</button> -->
		</form>
		<span id="quality"></span>
	</div>
</div>
</div>

<footer>
	<div class="col-lg-12 col-md-12 col-sm-12 pull-left footer text-center">
		521 Fifth Avenue, Suite 822, New York, NY 10175   |   +1 (646) 351-0511
	</div>
</footer>

<script type='text/javascript' src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type='text/javascript' src='https://192.168.4.20:8443/gateway/adapter.js'></script>
<script type='text/javascript' src='https://192.168.4.20:8443/gateway/fusion-client-sdk.js'></script>
<script type="text/javascript">

	 var token = '<?php echo $_SESSION['sessionid'] ?>';
	 // alert(token);
	 
	 UC.start(token);
	
		UC.phone.onLocalMediaStream = function(mediaStream){
			var localView = document.getElementById('local');
			UC.phone.setPreviewElement(localView);
		}

		$('#dial').click(function(e){
		 var numberDial = $('#dialNo').val();
		 // alert(numberDial);
		 
		 var call = UC.phone.createCall(numberDial);
		 
		 UC.phone.onRemoteMediaStream = function(mediaStream){
			var remoteView = document.getElementById('remote')[0];
			UC.phone.setVideoElement(remoteView);
		}
			call.dial();
		});
		
		$('#hangup').click(function(e){
			var calls = UC.phone.getCalls();
			var call = calls[0];
			if(call){
			call.end();
			}
		});
		
		UC.phone.onIncomingCall = function(newCall){
			
				swal({
				title: "Calling...",
				text: "Answer call from:" + newCall.getRemoteAddress() + '?',
				imageUrl: "image/call.png",
				showCancelButton: true,
				confirmButtonColor: "#dd6b55",
				confirmButtonText: "Accept"
			},
				function(){
					newCall.setPreviewElement(document.getElementById(local));
				    newCall.setVideoElement(document.getElementById(remote));
					newCall.answer();
				});
				
				
			
			
			
		};
		
   
	
</script>
</body>
</html>