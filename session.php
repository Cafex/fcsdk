<?php
	header('Access-Control-Allow-Origin: *');
	
	// start the session so we can access session vars
if (session_id() == '') {
    session_start();
}
	

	
	
	if (!isset($_SESSION['sessionid']) or isset($_GET['reset'])) {
	//configure the json to use in the session.
	
	$json = '

	{
		"webAppId": "b4062620-6e41-459e-93d0-f17cf342bd41",	
		"allowedOrgins": ["*"],
		"urlSchemeDetails": {
			"host": "192.168.4.20",
			"port": "8443",
			"secure": true
		},
		"voice":
		{
			"username": "admin",
			"displayName": "admin",
			"domain": "192.168.4.20",
			"inboundCallingEnabled": true
		}
	}
	';
	
	 // configure the curl options
    $ch = curl_init("http://192.168.4.20:8080/gateway/sessions/session");
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);	
    curl_setopt($ch, CURLOPT_HTTPHEADER, [         
        'Content-Type: application/json',
        'Content-Length: ' . strlen($json)
    ]);
    // execute HTTP POST & close the connection
    $response = curl_exec($ch);
    curl_close($ch);
    // decode the JSON and pick out the session token
    $decodedJson = json_decode($response);
    $id = $decodedJson->{'sessionid'};
	
	// store the web gateway session id
	$_SESSION['sessionid'] = $id;
	
    // echo the ID we've retrieved
   // echo $id;

	}
	
	session_destroy();
?>